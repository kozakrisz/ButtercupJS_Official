## Introduction

ButtercupJS is a lightweight tool for DOM modification instead of huge jQuery.

## Guide

**Select DOM Elements**

The default selector command:

`$B.select()`

If you want to select **more element by class name**, you can use:

`$B.select('.className', 'all')`

If you want to select **only the first matched element**, you can use:

`$B.select('.className', 'first')`

**Modify the selected elements**

If you want to **add class name(s)**, to the selected elements:

`$B.select('.className', 'all').addClass('.newClassName')`

*Of course you can use the 'first' selector instead of 'all' in the select section in this case also*

If you want to **remove class name**, to the selected elements:

`$B.select('.className', 'all').removeClass('.classNameYouWantToRemove')`

*Of course you can use the 'first' selector instead of 'all' in the select section in this case also*

If you want to **toggle class name**, to the selected elements:

`$B.select('.className', 'all').toggleClass('.classNameYouWantToToggle')`

*Of course you can use the 'first' selector instead of 'all' in the select section in this case also*

If you want to **check class name status**, on the selected element:

`$B.select('.className', 'first').hasClass('.classNameYouWantToCheck')` //true or false

*Of course you can use the 'first' selector instead of 'all' in the select section in this case also*

**You need only the elements?!**

`$B.select('.className', 'all').hasClass('.classNameYouWantToCheck').result` // array

*Of course you can use the 'first' selector instead of 'all' in the select section in this case also*

**Ajax/GET**

`$B.ajax('GET', 'https://jsonplaceholder.typicode.com/posts', function(data) {
    console.log(data);
});`





